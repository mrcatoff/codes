<?php


namespace App\Models;


use Core\DB;

/**
 * Class Phones
 * @package App\Models
 * @property-read int $id
 * @property string code
 * @property string country
 * @property string phone
 */
class Phones
{
    private $attributes = [];

    /**
     * Set attributes
     *
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if($name == 'id') {
            throw new \Exception('The identifier cannot be changed');
        }
        $this->attributes[$name] = $value;
    }

    /**
     * Get attributes
     *
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        }
        return null;
    }

    /**
     * Save phone number
     */
    public function save() {
        if($this->attributes['id']) {
            $sth = DB::prepare("UPDATE `phones` SET `code` = :code, `country` = :country, `phone` = :phone WHERE `id` = :id");
            $sth->bindParam(':code', $this->attributes['code'], \PDO::PARAM_STR);
            $sth->bindParam(':country', $this->attributes['country'], \PDO::PARAM_STR);
            $sth->bindParam(':phone', $this->attributes['phone'], \PDO::PARAM_STR);
            $sth->bindParam(':id', $this->attributes['id'], \PDO::PARAM_INT);
            $sth->execute();
        } else {
            $sth = DB::prepare("INSERT INTO `phones` (`code`, `country`, `phone`) VALUES (:code, :country, :phone)");
            $sth->bindParam(':code', $this->attributes['code'], \PDO::PARAM_STR);
            $sth->bindParam(':country', $this->attributes['country'], \PDO::PARAM_STR);
            $sth->bindParam(':phone', $this->attributes['phone'], \PDO::PARAM_STR);
            $sth->execute();
            $this->attributes['id'] = DB::lastInsertId();
        }
    }

    /**
     * Get total phone numbers
     *
     * @return int
     */
    public static function total() {
        $sth = DB::query('SELECT `id` FROM `phones`');
        return $sth->rowCount();
    }

    public static function count($code) {
        $sth = DB::prepare('SELECT `id` FROM `phones` WHERE `code` = :code');
        $sth->bindValue(':code', $code, \PDO::PARAM_STR);
        $sth->execute();
        return $sth->rowCount();
    }

    public static function min() {
        $sth = DB::prepare('SELECT `code`, COUNT(`id`) as `count` FROM `phones` GROUP BY `code` ORDER BY `count` ASC');
        $sth->execute();
        return $sth->fetch();
    }

    public static function max() {
        $sth = DB::prepare('SELECT `code`, COUNT(`id`) as `count` FROM `phones` GROUP BY `code` ORDER BY `count` DESC');
        $sth->execute();
        return $sth->fetch();
    }
}
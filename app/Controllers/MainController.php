<?php


namespace App\Controllers;

use App\Models\Phones;
use Core\Helpers;
use Core\PhonesCollection;

class MainController
{
    public function generate() {
        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            header('Content-Type: application/json');
            return json_encode([
                'status' => false,
                'message' => 'Некорректный запрос!'
            ]);
        }
        $_POST = json_decode(file_get_contents('php://input'), true);
        $total = filter_var($_POST['total'], FILTER_VALIDATE_INT);
        if($total == false) {
            return json_encode(['status' => false, 'message' => 'Количество должно быть числом',]);
        }

        $chunks = Helpers::chunk($total, count($_POST['codes']));
        $collection = new PhonesCollection();

        foreach ($_POST['codes'] as $key => $code) {
            foreach (Helpers::generate_phones($chunks[$key], $code['code']) as $generate_phone) {
                $phone = new Phones();
                $phone->country = $code['country'];
                $phone->code = $code['code'];
                $phone->phone = $generate_phone;
                $collection->addItem($phone);
            }
        }

        foreach ($collection->getIterator() as $item) {
            $item->save();
        }
        header('Content-Type: application/json');
        return json_encode([
            'status' => true,
        ]);
    }

    public function upload() {
        $total = Phones::total();
        $codes = [];
        foreach (array_keys(Helpers::$country_codes) as $code) {
            $codes[] = implode(',', [
                $code,
                Phones::count($code)
            ]);
        }
        $min_code = Phones::min();
        $max_code = Phones::max();
        $payload = [];
        $payload[] = implode(',', [
            'Total',
            $total
        ]);
        $payload[] = implode(',', [
            'Min code:',
            $min_code['code']
        ]);
        $payload[] = implode(',', [
            'Max code:',
            $max_code['code']
        ]);
        $payload = array_merge($payload, $codes);
        header('Content-Disposition: attachment;filename="'.date('Y-m-d_H_i_s').'.csv";');
        return implode("\n", $payload);
    }

    public function statistic() {
        $total = Phones::total();
        $codes = [];
        foreach (array_keys(Helpers::$country_codes) as $code) {
            $codes[] = [
                'code' => $code,
                'count' => Phones::count($code)
            ];
        }
        $min_code = Phones::min();
        $max_code = Phones::max();
        return json_encode([
            'total' => $total,
            'codes' => $codes,
            'min_code' => $min_code['code'],
            'max_code' => $max_code['code']
        ]);
    }
}
import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home";
import Statistic from "../views/Statistic";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/statistic",
        name: "Statistic",
        component: Statistic
    }
];

const router = new VueRouter({
    mode: "history",
    routes: routes
});

export default router;
import Vue from 'vue'
import App from "./components/App";
import router from "./router";

const app = new Vue({
    el: "#app",
    components: { App },
    router
});

window.app = app;

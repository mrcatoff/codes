<?php


namespace Core;


use App\Controllers\MainController;

class Route
{
    public function run() {
        if(
            strpos($_SERVER['REQUEST_URI'], 'uploads') !== false ||
            strpos($_SERVER['REQUEST_URI'], 'assets') !== false
        ) {
            return false;
        } else if(strpos($_SERVER['REQUEST_URI'], 'api')) {
            if(strpos($_SERVER['REQUEST_URI'], 'upload')) {
                $controller = new MainController();
                return $controller->upload();
            } else if(strpos($_SERVER['REQUEST_URI'], 'statistic')) {
                $controller = new MainController();
                return $controller->statistic();
            } else {
                $controller = new MainController();
                return $controller->generate();
            }
        }
        ob_start();
        require ROOT . 'app\view\main.php';
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }
}
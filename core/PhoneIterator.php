<?php

namespace Core;

use App\Models\Phones;
use Iterator;

/**
 * Class PhoneIterator
 */
class PhoneIterator implements Iterator
{
    /**
     * @var PhonesCollection
     */
    private $collection;

    /**
     * @var int Current position
     */
    private $position = 0;

    /**
     * PhoneIterator constructor.
     *
     * @param PhonesCollection $collection
     */
    public function __construct(PhonesCollection $collection)
    {
        $this->collection = $collection;
    }


    /**
     * Return the current element
     *
     * @return Phones
     */
    public function current()
    {
        return $this->collection->getItems()[$this->position];
    }

    /**
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
        $this->position = $this->position + 1;
    }

    /**
     * Return the key of the current element
     *
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Checks if current position is valid
     *
     * @return bool The return value will be casted to boolean and then evaluated.
     */
    public function valid()
    {
        return isset($this->collection->getItems()[$this->position]);
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
        $this->position = 0;
    }
}
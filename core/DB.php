<?php


namespace Core;


use Exception;
use PDO;
use PDOException;

/**
 * Class DB
 * @package Core
 */
class DB
{
    const DB_HOST = '185.86.76.146';
    const DB_USER = 'codes';
    const DB_PASSWORD = 'Y6g1A4q1';
    const DB_NAME = 'codes';
    const CHARSET = 'utf8';

    /**
     * @var PDO
     */
    private static $db;

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * DB constructor.
     * @throws Exception
     */
    public static function getInstance() {
        if (self::$instance === null){
            try {
                self::$db = new PDO(
                    'mysql:host='.self::DB_HOST.';dbname='.self::DB_NAME,
                    self::DB_USER,
                    self::DB_PASSWORD,
                    $options = [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".self::CHARSET
                    ]
                );
                self::$instance = new self();
            } catch (PDOException $e) {
                throw new Exception($e->getMessage());
            }
        }
        return self::$instance;
    }


    /**
     * Executes an SQL statement, returning a result set as a PDOStatement object
     *
     * @param $stmt
     * @return false|\PDOStatement
     */
    public static function query($stmt) {
        return self::$db->query($stmt);
    }

    /**
     * Prepares a statement for execution and returns a statement object
     *
     * @param string $stmt
     * @return bool|\PDOStatement
     */
    public static function prepare($stmt)  {
        return self::$db->prepare($stmt);
    }

    /**
     * Execute an SQL statement and return the number of affected rows
     *
     * @param string $query
     * @return false|int
     */
    public static function exec($query) {
        return self::$db->exec($query);
    }

    /**
     * Returns the ID of the last inserted row or sequence value
     *
     * @return int
     */
    static public function lastInsertId() {
        return self::$db->lastInsertId();
    }

    /**
     * Executes a prepared statement
     *
     * @param string $query
     * @param array $args
     * @return bool|false|\PDOStatement
     * @throws Exception
     */
    public static function run($query, $args = [])  {
        try{
            if (!$args) {
                return self::query($query);
            }
            $stmt = self::prepare($query);
            $stmt->execute($args);
            return $stmt;
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Execute a query and get one row from database
     *
     * @param $query
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public static function getRow($query, $args = [])  {
        return self::run($query, $args)->fetch();
    }

    /**
     * Execute a query and get more rows from database
     *
     * @param $query
     * @param array $args
     * @return array
     * @throws Exception
     */
    public static function getRows($query, $args = [])  {
        return self::run($query, $args)->fetchAll();
    }
}
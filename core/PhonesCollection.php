<?php


namespace Core;


use App\Models\Phones;
use IteratorAggregate;

/**
 * Class PhonesCollection
 */
class PhonesCollection implements IteratorAggregate
{
    private $items = [];

    /**
     * Get all items
     *
     * @return Phones[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Push new item to array
     *
     * @param $item
     */
    public function addItem($item)
    {
        $this->items[] = $item;
    }

    /**
     * Retrieve an external iterator
     *
     * @return PhoneIterator
     */
    public function getIterator()
    {
        return new PhoneIterator($this);
    }
}
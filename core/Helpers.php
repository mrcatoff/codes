<?php

namespace Core;

/**
 * Class Helpers
 */
class Helpers
{
    public static $country_codes = [
        '+1' => [
            'country'   => 'USA',
            'length'    => 10,
            'format'    => '+1-%d%d%d-%d%d%d-%d%d%d%d'
        ],
        '+44' => [
            'country'   => 'United Kingdom',
            'length'    => 10,
            'format'    => '+44 %d%d%d%d %d%d%d %d%d%d'
        ],
        '+61' => [
            'country'   => 'Australia',
            'length'    => 10,
            'format'    => '+61 %d%d%d%d %d%d%d %d%d%d'
        ],
        '+36' => [
            'country'   => 'Hungary',
            'length'    => 8,
            'format'    => '+36 %d%d %d%d%d %d%d%d'
        ],
        '+353' => [
            'country'   => 'Ireland',
            'length'    => 9,
            'format'    => '+353 %d%d %d%d%d %d%d%d%d'
        ],
        '+380' => [
            'country'   => 'Ukraine',
            'length'    => 9,
            'format'    => '+380 %d%d %d%d%d %d%d%d%d'
        ],
        '+7' => [
            'country'   => 'Russia',
            'length'    => 10,
            'format'    => '+7 %d%d%d %d%d%d %d%d%d%d'
        ],
        '+223' => [
            'country'   => 'Russia',
            'length'    => 10,
            'format'    => '+7 %d%d%d %d%d%d %d%d%d%d'
        ],
        '+373' => [
            'country'   => 'Moldova',
            'length'    => 8,
            'format'    => '+373 %d%d%d %d%d%d%d%d'
        ],
        '+687' => [
            'country'   => 'New Caledonia',
            'length'    => 6,
            'format'    => '+687 %d%d%d%d%d%d'
        ],
        '+51' => [
            'country'   => 'Peru',
            'length'    => 11,
            'format'    => '+51 %d%d %d%d%d%d%d%d%d%d%d'
        ]
    ];

    /**
     * Creates chunks of numbers
     *
     * @param int $number   Number to split
     * @param int $parts    Number of parts
     * @return array
     */
    public static function chunk($number, $parts) {
        if($parts == 1)
            return [$number];
        $chunks = [];
        $next = rand(0, ($number - $parts));
        $chunks[] = $next;

        $left = $number - $next;
        if($left == 0)
            return $chunks;

        $chunks = array_merge($chunks, self::chunk($left, $parts - 1));
        return $chunks;
    }

    public static function generate_phones($count, $code) {
        for ($i = 0; $i < $count; $i++) {
            $phone_numbers = [];
            for ($j = 0; $j < self::$country_codes[$code]['length']; $j++) {
                $phone_numbers[] = rand(0, 9);
            }
            yield sprintf(self::$country_codes[$code]['format'], ...$phone_numbers);
        }
    }
}
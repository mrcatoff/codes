<?php

define('ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
require ROOT . 'vendor/autoload.php';

try {
    \Core\DB::getInstance();
} catch (Exception $e) {
    return false;
}

\Core\DB::query(
    'CREATE TABLE IF NOT EXISTS `codes`.`phones` ( 
        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , 
        `code` VARCHAR(6) NOT NULL , 
        `country` VARCHAR(70) NOT NULL , 
        `phone` VARCHAR(15) NOT NULL , 
        PRIMARY KEY (`id`)
    ) ENGINE = InnoDB;');
/*use Core\Helpers;

foreach (Helpers::generate_phones(250, '+380') as $phone) {
    echo $phone . PHP_EOL;
}*/

$route = new \Core\Route();
$result = $route->run();
if(is_bool($result)) {
    return $result;
} else {
    echo $result;
}